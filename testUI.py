import os
import subprocess
import tkinter
import tkinter.messagebox
import customtkinter
import threading
from datetime import datetime 
import sys
import platform
import uuid
import tempfile
from tableauWorker import xmlParserV2 as tableau
from cognosWorker import cognosParser as cognos
from sapWorker import sapboParser as sap
import middleware

customtkinter.set_appearance_mode("Dark")  # Modes: "System" (standard), "Dark", "Light"
customtkinter.set_default_color_theme("blue")  # Themes: "blue" (standard), "green", "dark-blue"
old_stdout = sys.stdout

class App(customtkinter.CTk):
    
    def update_progressbar(self,totalFileCount,fileQueue):
        # simulate some work
        if self.progress_bar.get() == 1:
            self.progress_barset(0)
            return
        currentFileCount = fileQueue.qsize()
        self.progress_bar.set(((totalFileCount-currentFileCount)/(totalFileCount)))
        if self.progress_bar.get() < 1:
            self.after(500, self.update_progressbar, totalFileCount, fileQueue)
        else:
            self.status_label.configure(text="Completed")
            middleware.dumpQueues()
            # tkinter.filedialog.askopenfilename(initialdir=self.outputPath)
            self.popup_on_complete()
            # self.update_file_stats()
        # schedule the next update in 500 milliseconds
        # schedule the next update in 100 milliseconds
        # self.after(500, self.update_progressbar(totalFileCount=totalFileCount,fileQueue=fileQueue)
    
    def create_progressbar(self):
        self.progress_bar = customtkinter.CTkProgressBar(
            master = self,
            # orient='horizontal',
            mode='determinate',
            # length=440,
        )
        # place the progressbar
        self.progress_bar.place(relx=0.575,rely=0.7,relheight=0.05,relwidth=0.35,anchor=tkinter.CENTER)
        self.progress_bar.set(0)

    def popup_on_complete(self):
        self.process_complete_popup = customtkinter.CTkToplevel(self)
        self.process_complete_popup.title("Summary")
        self.process_complete_popup.geometry("300x150+{}+{}".format(int(self.winfo_x() + self.winfo_width()/2 - 200), int(self.winfo_y() + self.winfo_height()/2 - 100)))
        label = customtkinter.CTkLabel(self.process_complete_popup,text=f"Files Selected= {self.totalFilesSelected}\n\nFiles Processed= {self.seleced_queue_size}\n\nInvalid Files= {self.totalFilesSelected - self.seleced_queue_size}")
        label.pack(padx=10, pady=10)
        ok_button = customtkinter.CTkButton(self.process_complete_popup, text="OK", command=self.clicked_on_ok)
        ok_button.pack(side=customtkinter.LEFT, padx=10, pady=10)
        invf_button = customtkinter.CTkButton(self.process_complete_popup, text="Invalid Files", command=self.open_invalid_file)
        invf_button.pack(side=customtkinter.RIGHT, padx=10, pady=10)
        self.process_complete_popup.grab_set()
    
    def login_sap_popup(self):
        self.sap_login_popup = customtkinter.CTkToplevel(self)
        self.sap_login_popup.title("SAP Login")
        self.sap_login_popup.geometry("370x180+{}+{}".format(int(self.winfo_x() + self.winfo_width()/2 - 200), int(self.winfo_y() + self.winfo_height()/2 - 100)))
        # label = customtkinter.CTkLabel(self.popup,text=f"Files Selected= {self.totalFilesSelected}\n\nFiles Processed= {self.seleced_queue_size}\n\nInvalid Files= {self.totalFilesSelected - self.seleced_queue_size}")
        server_name_label = customtkinter.CTkLabel(self.sap_login_popup,text="Server:")
        server_name_label.grid(row = 0,column = 0,padx=7, pady=7)
        server_name_label.place(relx=0.1,rely=0.03)
        self.server_name = customtkinter.CTkEntry(self.sap_login_popup,width=250)
        self.server_name.grid(row = 0,column = 1,padx=7, pady=7)
        self.server_name.place(relx=0.30,rely=0.03)
        user_name_label = customtkinter.CTkLabel(self.sap_login_popup,text="Username:")
        user_name_label.grid(row = 1,column = 0,padx=7, pady=7)
        user_name_label.place(relx=0.1,rely=0.25)
        self.user_name = customtkinter.CTkEntry(self.sap_login_popup,width=250)
        self.user_name.grid(row = 1,column = 1,padx=7, pady=7)
        self.user_name.place(relx=0.30,rely=0.25)
        password_label = customtkinter.CTkLabel(self.sap_login_popup,text="Password:")
        password_label.grid(row = 2,column = 0,padx=7, pady=7)
        password_label.place(relx=0.1,rely=0.47)
        self.password = customtkinter.CTkEntry(self.sap_login_popup,width=250)
        self.password.grid(row = 2,column = 1,padx=7, pady=7)
        self.password.place(relx=0.30,rely=0.47)
        submit_button = customtkinter.CTkButton(self.sap_login_popup, text="Submit", command=self.select_output_path_SAP)
        submit_button.grid(row = 3,column = 0, padx=7, pady=7)
        submit_button.place(relx=0.05,rely=0.75)
        cancel_button = customtkinter.CTkButton(self.sap_login_popup, text="Cancel",command=lambda: [self.sap_login_popup.destroy(),self.reset_main()])
        cancel_button.grid(row = 3,column = 1, padx=7, pady=7)
        cancel_button.place(relx=0.57,rely=0.75)
        self.sap_login_popup.grab_set()

    def select_output_path_SAP(self):
        try:
            self.SAPWorker = sap.BIPReport(self.server_name.get(),self.user_name.get(),self.password.get())
 
            self.sap_login_popup.destroy()
            self.sap_output_path_popup = customtkinter.CTkToplevel(self)
            self.sap_output_path_popup.title("Select Output Path")
            self.sap_output_path_popup.geometry("550x70+{}+{}".format(int(self.winfo_x() + self.winfo_width()/2 - 200), int(self.winfo_y() + self.winfo_height()/2 - 100)))
            output_file_path_label = customtkinter.CTkLabel(self.sap_output_path_popup,text="Output File Path:")
            output_file_path_label.grid(row = 0,column = 0,padx=7, pady=7)
            output_file_path_label.place(relx=0.02,rely=0.3)
            self.output_file_path = customtkinter.CTkEntry(self.sap_output_path_popup,width=250)
            self.output_file_path.grid(row = 0,column = 1,padx=7, pady=7)
            self.output_file_path.place(relx=0.22,rely=0.3)
            select_output_button= customtkinter.CTkButton(self.sap_output_path_popup,text="...",width=37,command=self.select_output_dir)
            select_output_button.grid(row=0,column=3,padx=7, pady=7)
            select_output_button.place(relx=0.7,rely=0.3)
            submit_button = customtkinter.CTkButton(self.sap_output_path_popup, text="Submit",width=85,command=lambda: [self.open_input_dialog_event() , self.sap_output_path_popup.destroy()])
            submit_button.grid(row = 0,column = 4, padx=7, pady=7)
            submit_button.place(relx=0.8,rely=0.3)
            self.sap_output_path_popup.grab_set()
        
        except ValueError:
            self.disp_error("Server Name cannot be Empty")

        except Exception as e:
            self.disp_error(f"Unable to connect with Server \n Username or Password might be wrong")

    def select_output_dir(self):
        self.output_file_path.delete(0,customtkinter.END)
        self.folder = tkinter.filedialog.askdirectory(initialdir="/", title="Select Folder")
        self.output_file_path.insert(0,self.folder)  
         
    
    # def sap_check_error(self):
    #     error_msg=""
    #     flag=0
    #     if(self.server_name.index("end") == 0):
    #         error_msg="Server Name is Required"
    #         flag=1
    #     elif(self.user_name.index("end") == 0):
    #         error_msg="User Name is Required"
    #         flag=1
    #     elif(self.password.index("end") == 0):
    #         error_msg="Password is Required"
    #         flag=1
    #     else:
    #         error_msg="all Okay"
    #     print(error_msg)
    #     # print("submit clicked")
    #     if(flag==0):
    #         self.select_output_path_SAP()
    #     else:
    #         self.disp_error(error_msg)




    def disp_error(self,msg):
        self.error_popup = customtkinter.CTkToplevel(self)
        self.error_popup.title("Error")
        self.error_popup.geometry("350x100+{}+{}".format(int(self.winfo_x() + self.winfo_width()/2 - 200), int(self.winfo_y() + self.winfo_height()/2 - 100)))
        error_label = customtkinter.CTkLabel(self.error_popup,text=f"Error: {msg}")
        error_label.grid(row = 0,column = 2,padx=7, pady=7)
        error_label.place(relx=0.19,rely=0.1)
        ok_button = customtkinter.CTkButton(self.error_popup, text="OK",width=65, command=self.destroy_error)
        ok_button.grid(row = 1,column = 1, padx=7, pady=7)
        ok_button.place(relx=0.42,rely=0.6)
        self.error_popup.grab_set()

    def destroy_error(self):
        self.error_popup.destroy()


    def clicked_on_ok(self):
        self.process_complete_popup.destroy()
        if sys.platform == 'win32':
            subprocess.Popen(['start', '', self.outputPath], shell=True)
        else:
            subprocess.Popen(['open', '-R', self.outputPath])

        # tkinter.filedialog.askopenfilename(initialdir=self.outputPath)
        self.progress_bar.place_forget()
        self.status_label.configure(text="")
        

    def open_invalid_file(self):
        if middleware.logPath == None or middleware.logPath == "":
            return
        file_path = os.path.join(middleware.logPath, "invalidfiles.txt")
        if file_path:
            subprocess.run(['start', '', file_path], shell=True)

    def startSAPWorker(self):
        self.open_input_dialog_event()
        self.reset_main()     

    def update_file_stats(self):
        self.totalFilesSelected = middleware.tableauFileQueue.qsize() + middleware.cognosFileQueue.qsize() + middleware.qlikFileQueue.qsize() + middleware.unknownFileQueue.qsize()
        # self.sidebar_total_stats.configure(text=f"Selected Files - {self.totalFilesSelected}")
        match self.buttonClicked:
            
            case 1:
                self.seleced_queue_size = middleware.tableauFileQueue.qsize()
                # self.sidebar_current_stats.configure(text=f"Tableau Files - {middleware.tableauFileQueue.qsize()}")
                # self.sidebar_invalid_stats.configure(text=f"Invalid Files - {self.totalFilesSelected - middleware.tableauFileQueue.qsize()}")
            
            case 2:
                self.seleced_queue_size = middleware.cognosFileQueue.qsize()
                # self.sidebar_current_stats.configure(text=f"Cognos Files - {middleware.cognosFileQueue.qsize()}")
                # self.sidebar_invalid_stats.configure(text=f"Invalid Files - {self.totalFilesSelected - middleware.cognosFileQueue.qsize()}")
            
            case 3:
                self.seleced_queue_size = middleware.qlikFileQueue.qsize()
                # self.sidebar_current_stats.configure(text=f"Qlik Files - {middleware.qlikFileQueue.qsize()}")
                # self.sidebar_invalid_stats.configure(text=f"Invalid Files - {self.totalFilesSelected - middleware.qlikFileQueue.qsize()}")
                self.status_label.configure(text="WIP")
            
            case 4:
                self.seleced_queue_size = 0
                # self.sidebar_current_stats.configure(text=f"SAP BO Files - {middleware.sapboFileQueue.qsize()}")
                # self.sidebar_invalid_stats.configure(text=f"Invalid Files - {self.totalFilesSelected - middleware.sapboFileQueue.qsize()}")
                self.status_label.configure(text="WIP")
            case default:
                self.status_label.configure(text="WIP")
                return
                
    def select_file_dialog(self):
        self.main_button_1.configure(state="disabled")
        self.dialog = tkinter.filedialog.askdirectory(initialdir="/", title="Select Folder")
        print(f"selected Folder-{self.dialog}")
        self.status_label.configure(text="Reading Files")
        middleware.readFiles(self.dialog)
        self.update_file_stats()
        self.create_progressbar()
    def reset_main(self):
        self.sidebar_button_1.destroy()
        self.sidebar_button_2.destroy()
        self.sidebar_button_3.destroy()
        self.sidebar_button_4.destroy()
        self.create_button()
        self.status_label.configure(text="")
        self.progress_bar.place_forget()

    def open_input_dialog_event(self):
        self.sidebar_button_1.destroy()
        self.sidebar_button_2.destroy()
        self.sidebar_button_3.destroy()
        self.sidebar_button_4.destroy()
        self.create_button()
        self.outputPath = ""
        match self.buttonClicked:
            case 1:
                self.select_file_dialog()
                print("Clicked Tableau")
                self.status_label.configure(text="Processing")
                totalFilesCount = middleware.tableauFileQueue.qsize()+1
                # tableau.main(middleware.tableauFileQueue)
                threading.Thread(target=tableau.main, args=(middleware.tableauFileQueue,)).start()
                self.update_progressbar(totalFilesCount,middleware.tableauFileQueue)
                # tkinter.filedialog.askopenfilename(initialdir=tableau.outputPath)
                # self.status_label.configure(text="Completed")
                self.outputPath = os.path.join(self.dialog,"tableauOutput")
            case 2:
                self.select_file_dialog()
                print("Clicked Cognos")
                self.status_label.configure(text="Processing")
                totalFilesCount = middleware.cognosFileQueue.qsize()+1
                threading.Thread(target=cognos.mainCode, args=(middleware.cognosFileQueue,)).start()
                self.update_progressbar(totalFilesCount,middleware.cognosFileQueue)
                # tkinter.filedialog.askopenfilename(initialdir=cognos.outputPath)
                # self.status_label.configure(text="Completed")
                self.outputPath = os.path.join(self.dialog,"cognosOutput")
            case 3:
                print("Clicked Qlik")
                self.status_label.configure(text="WIP")
                # self.status_label.configure(text="Running")
                # self.status_label.configure(text="Completed")
            case 4:
                print("Clicked SAP BO")
                self.status_label.configure(text="Processing")
                self.outputPath = self.output_file_path.get()
                threading.Thread(target=sap.main, args=(self.SAPWorker,self.outputPath)).start()
                # self.status_label.configure(text="Running")
                # self.status_label.configure(text="Completed")
        # self.update_file_stats()
        # tkinter.filedialog.askopenfilename(initialdir=self.outputPath)


    def change_appearance_mode_event(self, new_appearance_mode: str):
        customtkinter.set_appearance_mode(new_appearance_mode)
        # new_scaling_float = int(new_scaling.replace("%", "")) / 100
        # customtkinter.set_widget_scaling(new_scaling_float)

    def selectDisplayMode(self):
        self.appearance_mode_label = customtkinter.CTkLabel(self.sidebar_frame, text="Appearance Mode:", anchor="w")
        self.appearance_mode_label.grid(row=11, column=0, padx=20, pady=(10, 0))
        
        self.appearance_mode_optionemenu = customtkinter.CTkOptionMenu(self.sidebar_frame, text_color="white", values=["Dark", "System", "Light"],
                                                                       command=self.change_appearance_mode_event)
        self.appearance_mode_optionemenu.grid(row=12, column=0, padx=20, pady=(10, 10))

    def create_button(self):
        self.sidebar_button_1 = customtkinter.CTkButton(self.sidebar_frame, text="Tableau", text_color="white", command=lambda: [self.sidebar_button_event(1)])
        self.sidebar_button_1.grid(row=1, column=0, padx=20, pady=10)
        
        self.sidebar_button_2 = customtkinter.CTkButton(self.sidebar_frame, text="Cognos", text_color="white", command=lambda: [self.sidebar_button_event(2)])
        self.sidebar_button_2.grid(row=2, column=0, padx=20, pady=10)
        
        self.sidebar_button_3 = customtkinter.CTkButton(self.sidebar_frame, text="Qlik", text_color="white", command=lambda: [self.sidebar_button_event(3)])
        self.sidebar_button_3.grid(row=3, column=0, padx=20, pady=10)
        
        self.sidebar_button_4 = customtkinter.CTkButton(self.sidebar_frame, text="SAP BO", text_color="white", command=lambda: [self.sidebar_button_event(4)])
        self.sidebar_button_4.grid(row=4, column=0, padx=20, pady=10)

        # self.sidebar_stats = customtkinter.CTkLabel(self.sidebar_frame, text="File Statistics")
        # self.sidebar_frame.configure
        # self.sidebar_stats.grid(row=5, column=0)
        # self.totalFilesSelected = middleware.tableauFileQueue.qsize() + middleware.cognosFileQueue.qsize() + middleware.qlikFileQueue.qsize() + middleware.sapboFileQueue.qsize() + middleware.unknownFileQueue.qsize()
        # self.sidebar_total_stats = customtkinter.CTkLabel(self.sidebar_frame, text=f"Selected Files - {self.totalFilesSelected}")
        # self.sidebar_total_stats.grid(row=5, column=0)
        # self.sidebar_current_stats = customtkinter.CTkLabel(self.sidebar_frame, text="")
        # self.sidebar_current_stats.grid(row=6, column=0)
        # self.sidebar_invalid_stats = customtkinter.CTkLabel(self.sidebar_frame, text="")
        # self.sidebar_invalid_stats.grid(row=7, column=0)
        # self.sidebar_sapbo_stats = customtkinter.CTkLabel(self.sidebar_frame, text=f"SAP BO Files - {middleware.sapboFileQueue.qsize()}")
        # self.sidebar_sapbo_stats.grid(row=8, column=0)
        # self.sidebar_unknown_stats = customtkinter.CTkLabel(self.sidebar_frame, text=f"Unknown Files - {middleware.unknownFileQueue.qsize()}")
        # self.sidebar_unknown_stats.grid(row=9, column=0)
        # print(dir(middleware))
        # start_button = customtkinter.CTkButton(
        #     master = self,
        #     text='Start',
        #     command=None
        # )
        # start_button.grid(column=7, row=8, padx=10, pady=10)
        # stop_button = customtkinter.CTkButton(
        #     master = self,
        #     text='Stop',
        #     command=None
        # )
        # stop_button.grid(column=8, row=8, padx=10, pady=10)

        # progress_bar = ttk.Progressbar(
        #     master = self,
        #     orient='horizontal',ready
        #     mode='determinate',
        #     length=340
        # )
        # place the progressbar
        # progress_bar.grid(row=2, padx=10, pady=20,columnspan=8)
        return True
    
    def sidebar_button_event(self,button):
        self.main_button_1.configure(state="enabled")
        middleware.clearQueues()
        # self.status_label.configure(text="READY")
        match button:
            case 1:
                self.sidebar_button_1.configure(fg_color="green")
                self.sidebar_button_2.destroy()
                self.sidebar_button_3.destroy()
                self.sidebar_button_4.destroy()
                self.sidebar_button_2 = customtkinter.CTkButton(self.sidebar_frame, text="Cognos", text_color="white", command=lambda: [self.sidebar_button_event(2)])
                self.sidebar_button_2.grid(row=2, column=0, padx=20, pady=10)
                self.sidebar_button_3 = customtkinter.CTkButton(self.sidebar_frame, text="Qlik", text_color="white", command=lambda: [self.sidebar_button_event(3)])
                self.sidebar_button_3.grid(row=3, column=0, padx=20, pady=10)
                self.sidebar_button_4 = customtkinter.CTkButton(self.sidebar_frame, text="SAP BO", text_color="white", command=lambda: [self.sidebar_button_event(4)])
                self.sidebar_button_4.grid(row=4, column=0, padx=20, pady=10)
                self.buttonClicked = 1
                # self.update_file_stats()
            case 2:
                self.sidebar_button_2.configure(fg_color="green")
                self.sidebar_button_1.destroy()
                self.sidebar_button_3.destroy()
                self.sidebar_button_4.destroy()
                self.sidebar_button_1 = customtkinter.CTkButton(self.sidebar_frame, text="Tableau", text_color="white", command=lambda: [self.sidebar_button_event(1)])
                self.sidebar_button_1.grid(row=1, column=0, padx=20, pady=10)
                self.sidebar_button_3 = customtkinter.CTkButton(self.sidebar_frame, text="Qlik", text_color="white", command=lambda: [self.sidebar_button_event(3)])
                self.sidebar_button_3.grid(row=3, column=0, padx=20, pady=10)
                self.sidebar_button_4 = customtkinter.CTkButton(self.sidebar_frame, text="SAP BO", text_color="white", command=lambda: [self.sidebar_button_event(4)])
                self.sidebar_button_4.grid(row=4, column=0, padx=20, pady=10)
                self.buttonClicked = 2
                # self.update_file_stats()
            case 3:
                self.sidebar_button_3.configure(fg_color="green")
                self.sidebar_button_2.destroy()
                self.sidebar_button_1.destroy()
                self.sidebar_button_4.destroy()
                self.sidebar_button_2 = customtkinter.CTkButton(self.sidebar_frame, text="Cognos", command=lambda: [self.sidebar_button_event(2)])
                self.sidebar_button_2.grid(row=2, column=0, padx=20, pady=10)
                self.sidebar_button_1 = customtkinter.CTkButton(self.sidebar_frame, text="Tableau", command=lambda: [self.sidebar_button_event(1)])
                self.sidebar_button_1.grid(row=1, column=0, padx=20, pady=10)
                self.sidebar_button_4 = customtkinter.CTkButton(self.sidebar_frame, text="SAP BO", text_color="white", command=lambda: [self.sidebar_button_event(4)])
                self.sidebar_button_4.grid(row=4, column=0, padx=20, pady=10)
                self.buttonClicked = 3
                # self.update_file_stats()
            case 4:
                self.sidebar_button_4.configure(fg_color="green")
                self.sidebar_button_2.destroy()
                self.sidebar_button_1.destroy()
                self.sidebar_button_3.destroy()
                self.sidebar_button_1 = customtkinter.CTkButton(self.sidebar_frame, text="Tableau", text_color="white", command=lambda: [self.sidebar_button_event(1)])
                self.sidebar_button_1.grid(row=1, column=0, padx=20, pady=10)
                self.sidebar_button_2 = customtkinter.CTkButton(self.sidebar_frame, text="Cognos", text_color="white", command=lambda: [self.sidebar_button_event(2)])
                self.sidebar_button_2.grid(row=2, column=0, padx=20, pady=10)
                self.sidebar_button_3 = customtkinter.CTkButton(self.sidebar_frame, text="Qlik", text_color="white", command=lambda: [self.sidebar_button_event(3)])
                self.sidebar_button_3.grid(row=3, column=0, padx=20, pady=10)
                self.buttonClicked = 4
                self.main_button_1.configure(state="disabled")
                self.login_sap_popup()
                # self.update_file_stats()

    def createSidebarFrame(self):
        # self.sidebar_frame_1 = tkinter.Frame(self, bg="red")#, corner_radius=0)
        # self.sidebar_frame_1.grid(row=0, column=0, rowspan=4, sticky="nsew")
        # self.sidebar_frame_1.place(relwidth=0.164, relheight=0.1)
        
        self.sidebar_frame = customtkinter.CTkFrame(self, width=140, corner_radius=0)
        self.sidebar_frame.grid(row=0, column=0, rowspan=6, sticky="nsew")
        self.sidebar_frame.grid_rowconfigure(10, weight=1)
        # self.sidebar_frame.place(rely=0,relwidth=0.165, relheight=1)
        # self.sidebar_frame_logo = tkinter.Frame(self, width=140,bg="red")
        # self.sidebar_frame.grid(row=1, column=0, rowspan=4, sticky="nsew")
        
    def selectDirectory(self):
        self.main_button_1 = customtkinter.CTkButton(master=self, command=self.open_input_dialog_event,fg_color="transparent", text="Open Folder",border_width=2, text_color=("gray10", "#DCE4EE"))
        # self.grid_rowconfigure(8, weight=1)
        # self.rowconfigure(8)
        # self.columnconfigure(8)
        # self.grid_columnconfigure(8,weight=1)
        self.main_button_1.grid(row=1, column=2, padx=(20, 20), pady=(20, 20), sticky="nsew")
        self.main_button_1.place(relx=0.575,rely=0.5,relheight=0.1,relwidth=0.15,anchor=tkinter.CENTER)
        self.main_button_1.configure(state="disabled")
        self.status_label = customtkinter.CTkLabel(self, text=f"{self.current_state}")
        self.status_label.place(relx=0.575,rely=0.75,relheight=0.05,relwidth=0.35,anchor=tkinter.CENTER)
        # start_button = customtkinter.CTkButton(
        #     master = self,
        #     text='Start',
        #     command= self.click_on_start_event  #self.click_on_start_event
        # )
        # start_button.place(relx=0.745,rely=0.95,relheight=0.05,relwidth=0.1,anchor=tkinter.SW)
        # stop_button = customtkinter.CTkButton(
        #     master = self,
        #     text='Stop',
        #     command=None
        # )
        # stop_button.place(relx=0.865,rely=0.95,relheight=0.05,relwidth=0.1,anchor=tkinter.SW)

    def __init__(self):
        super().__init__()
        self.buttonClicked = None
        self.current_state = ""
        # configure window
        self.title("Reports Analysis Automation")
        self.geometry(f"{1100}x{600}")
        
        # set maximum window size value
        self.resizable(False, False)

        # configure grid layout (4x4)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure((2, 3), weight=0)
        self.grid_rowconfigure((0, 1, 2), weight=1)

        # create sidebar frame with widgets
        self.createSidebarFrame()
        
        self.logo_label = customtkinter.CTkLabel(self.sidebar_frame, text="Go Power-BI", height=40,width=200, text_color="yellow", font=customtkinter.CTkFont(size=20, weight="bold"))
        self.logo_label.grid(row=0, column=0)

        # creating debug.log file 
        logPath = os.path.join(tempfile.gettempdir(),"Reports Analysis Automation"+str(uuid.uuid4()))
        try:
                os.mkdir(logPath)
                middleware.logPath = logPath
                self.log_file = open(os.path.join(logPath,"debug.log"),"a")
        except OSError:
            print("Can't Write to LOG Directory.")
        # sys.stdout = self.log_file
        print("\n\n\n")
        print("-------------------------------------------")
        print("Timestamp = ",datetime.now())
        print('Operating system: %s %s', platform.system(), platform.release())
        print('Python version: %s (%s)', platform.python_version(), platform.python_implementation())
        # create buttons
        self.create_button()

        # select folder
        self.selectDirectory()
        
        # select dsiplay mode
        self.selectDisplayMode()
    
    
    def __del__(self):
        # self.log_file.write("\n\n\n")
        self.log_file.close()


if __name__ == "__main__":
    app = App()
    app.mainloop()