
# importing the module
import tracemalloc
from . import xmlParserV2 as driver
import time
# code or function for which memory
# has to be monitored

 
# starting the monitoring
tracemalloc.start()
 
# function call
start = time.time()
folderPath = r"C:\Users\MAQ\Documents\Code\TWB Files"
driver.main(folderPath)
end = time.time()
# displaying the memory
print(tracemalloc.get_traced_memory())
print("The time of execution of above program is :",
      (end-start) * 10**3, "ms")
# stopping the library
tracemalloc.stop()