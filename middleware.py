import queue
import os
import tableauWorker as tableau
import cognosWorker as cognos

tableauFileQueue = queue.Queue()
cognosFileQueue = queue.Queue()
unknownFileQueue = queue.Queue()
qlikFileQueue = queue.Queue()
# sapboFileQueue = queue.Queue()
logPath = ""
def clearQueues():
    with tableauFileQueue.mutex:
        tableauFileQueue.queue.clear()
    with cognosFileQueue.mutex:
        cognosFileQueue.queue.clear()
    with unknownFileQueue.mutex:
        unknownFileQueue.queue.clear()
    with qlikFileQueue.mutex:
        qlikFileQueue.queue.clear()
    # with sapboFileQueue.mutex:
    #     sapboFileQueue.queue.clear()

def dumpQueues():
    if logPath == None or logPath == None:
        clearQueues()
        return
    invalidFiles = open(os.path.join(logPath,"invalidfiles.txt"),"w")
    print("[-] Invalid Files")
    invalid = []
    while not tableauFileQueue.empty():
        item = tableauFileQueue.get()
        invalid.append(item+"/n")
        invalidFiles.writelines
        print(item)

    while not cognosFileQueue.empty():
        item = cognosFileQueue.get()
        invalid.append(item+"\n")
        print(item)

    while not unknownFileQueue.empty():
        item = unknownFileQueue.get()
        invalid.append(item+"\n")
        print(item)

    while not qlikFileQueue.empty():
        item = qlikFileQueue.get()
        invalid.append(item+"\n")
        print(item)
    while not qlikFileQueue.empty():
        item = qlikFileQueue.get()
        invalid.append(item+"\n")
        print(item)
    invalidFiles.writelines(invalid)
    invalidFiles.close()

def readFiles(folderPath):
    for file in os.listdir(folderPath):
        if os.path.isfile(os.path.join(folderPath, file)):
            firstLine = ""
            if os.path.isfile(os.path.join(folderPath, file)):
                try:
                    with open(os.path.join(folderPath, file), 'r') as content:
                        lines = []
                        for i in range(5):
                            try:
                                line = content.readline()
                                if not line:
                                    break
                            except (IOError, UnicodeDecodeError) as e:
                                print('[-] Error reading from:',str(os.path.join(folderPath, file)), str(e))
                                break
                            lines.append(line.strip())
                        firstLine = '\n'.join(lines)
                except FileNotFoundError:
                    print('[-] File not found.')
                except IOError as e:
                    print('[-] Error opening file:', str(e))
                    # print(firstLine)
            if file.endswith('.twb') and 'tableau' in firstLine:
                tableauFileQueue.put(os.path.join(folderPath, file))
            elif file.endswith('.xml') and 'cognos' in firstLine:
                cognosFileQueue.put(os.path.join(folderPath, file))
            else:
                unknownFileQueue.put(os.path.join(folderPath, file))


# folderPath = r"C:\Users\MAQ\Documents\Code\TWB Files"
# readFiles(folderPath)