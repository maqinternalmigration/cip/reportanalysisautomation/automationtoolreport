import pandas as pd
import re
hello = "World"
def getNamespace(elem):
  if elem.tag[0] == "{":
      uri, ignore, tag = elem.tag[1:].partition("}")
  else:
      uri = None
      tag = elem.tag
  return uri


def getTwoClause(st):
  req = set()
  start = 0
  stack = []
  count = 0
  for i in range(0,len(st)):
    if(st[i]=="[" and len(stack)==0):
      if(count == 0):
        start = i
      stack.append("[")
    elif(i+1<len(st) and st[i] == "]" and st[i+1]!="." and len(stack)!=0 and count!=1):
      stack.pop()
      count = 0
    elif(i+1<len(st) and st[i] == "]" and st[i+1]!="." and len(stack)!=0 and count==1):
      stack.pop()
      end = i
      req.add(st[start:i+1])
      count = 0
    elif(i+1<len(st) and st[i] == "]" and st[i+1]=="." and len(stack)!=0 and count==1 and i+2<len(st) and st[i+2]!="["):
      stack.pop()
      end = i
      req.add(st[start:i+1])
      count = 0
    elif(i+1==len(st) and st[i] == "]"  and len(stack)!=0 and count==1):
      stack.pop()
      end = i
      req.add(st[start:i+1])
      count = 0
    elif(i+1<len(st) and st[i] == "]" and st[i+1] =="." and len(stack)!=0):
      stack.pop()
      count = count+1
      if(i+2<len(st) and st[i+2] != '['):
        count = 0
  return req


def getOneClause(st):
  req = set()
  start = 0
  stack = []
  count = 0
  for i in range(0,len(st)):
    # print(st[i],count)
    if(st[i]=="[" and len(stack)==0):
      if(count == 0):
        start = i
      stack.append("[")
    elif(i+1<len(st) and st[i] == "]" and st[i+1]!="." and len(stack)!=0 and count!=0):
      stack.pop()
      count = 0
    elif(i+1<len(st) and st[i] == "]" and st[i+1]!="." and len(stack)!=0 and count==0):
      stack.pop()
      end = i
      req.add(st[start:i+1])
      count = 0
    elif(i+1<len(st) and st[i] == "]" and st[i+1]=="." and len(stack)!=0 and count==0 and i+2<len(st) and st[i+2]!="["):
      stack.pop()
      end = i
      req.add(st[start:i+1])
      count = 0
    elif(i+1==len(st) and st[i] == "]"  and len(stack)!=0 and count==0):
      stack.pop()
      end = i
      req.add(st[start:i+1])
      count = 0
    elif(i+1<len(st) and st[i] == "]" and st[i+1] =="." and len(stack)!=0):
      stack.pop()
      count = count+1
      if(i+2<len(st) and st[i+2] != '['):
        count = 0
  return req

def QueryDataItemsPage(namespace,root):
  dict ={}

  for queries in root:
    if(re.sub(namespace, '', queries.tag) == 'queries'):  
      for query in queries:  
        if(re.sub(namespace, '', query.tag) == 'query'):
          for selection in query:
            if(re.sub(namespace, '', selection.tag) == 'selection'):
              dataItemDict = {}
              for dataItem in selection:
                if(re.sub(namespace, '', dataItem.tag) == 'dataItem'):
                  for expressions in dataItem:
                    if(re.sub(namespace, '', expressions.tag) == 'expression'):
                      dataItemDict[dataItem.attrib['name']] = expressions.text
              dict[query.attrib['name']] = dataItemDict
  # Creating a dictonary for quick addition to a dataframe for csv export
  # Adding the expression like [*].[*].[*] to a new column as a list
  transferdictWithExp = {'query_name': [], 'dataItem_name': [], 'expression': [], 'data_objects': []}
  for i in dict:
    for j in dict[i]:
      # print(i, "   ",j, "   ",dict[i][j])
      k1 = set(re.findall(r"\[[a-zA-Z0-9_ ]*\]\.\[[a-zA-Z0-9_ ]*\]\.\[[a-zA-Z0-9_ ]*\]",dict[i][j]))
      k2 = getTwoClause(dict[i][j])
      k3 = getOneClause(dict[i][j])
      if(len(k1)):
        for k in k1:
          transferdictWithExp['query_name'].append(i.strip())
          transferdictWithExp['dataItem_name'].append(j.strip())
          transferdictWithExp['expression'].append(dict[i][j].strip())
          l = k.replace('[','')
          l = l.replace(']','')
          transferdictWithExp['data_objects'].append(l)
      if(len(k2)):
        for k in k2:
          transferdictWithExp['query_name'].append(i.strip())
          transferdictWithExp['dataItem_name'].append(j.strip())
          transferdictWithExp['expression'].append(dict[i][j].strip())
          l = k.replace('[','')
          l = l.replace(']','')
          transferdictWithExp['data_objects'].append(l)
      if(len(k3)):
        for k in k3:
          transferdictWithExp['query_name'].append(i.strip())
          transferdictWithExp['dataItem_name'].append(j.strip())
          transferdictWithExp['expression'].append(dict[i][j].strip())
          l = k.replace('[','')
          l = l.replace(']','')
          transferdictWithExp['data_objects'].append(l)
        
      if(len(k1) ==0 and len(k2)==0 and len(k3)==0):
        transferdictWithExp['query_name'].append(i.strip())
        transferdictWithExp['dataItem_name'].append(j.strip())
        transferdictWithExp['expression'].append(dict[i][j].strip())
        transferdictWithExp['data_objects'].append("")

  df_query_dataItems = pd.DataFrame(transferdictWithExp)
  return df_query_dataItems


def getPageItems(namespace,root,tempList,myList,lastRefQuery,prefixIdentifier,prefixContentType,pageNameList,firstRefQuery,isMasterContext,seenContents):
  if('refQuery' in root.attrib):
    lastRefQuery = root.attrib['refQuery'] 
  if('refQuery' in root.attrib and firstRefQuery == "" and seenContents == 1):
    firstRefQuery = root.attrib['refQuery']
  if(re.sub(namespace, '', root.tag) == 'masterContext'):
    isMasterContext = 1
  if(re.sub(namespace, '', root.tag) == 'textItem'):
    for j in root:
      if(re.sub(namespace, '', j.tag) == 'dataSource'):
        for k in j:
          if(re.sub(namespace, '', k.tag) == 'staticValue'):
            newtempList = list(tempList)
            newtempList.append('staticValue')
            newtempList.append(k.text)
            if(isMasterContext):
              #if it's next to master Context it's referred query will be the first ref query in the traversal
              newtempList.append(firstRefQuery)
            else:
              newtempList.append(lastRefQuery)  
            newtempList.append(prefixContentType+str(prefixIdentifier)[0])
            if(len(newtempList)==6):
              myList.append(newtempList)
  elif('refDataItem' in root.attrib):
    newtempList = list(tempList)
    newtempList.append('refDataItem')
    newtempList.append(root.attrib['refDataItem'])
    if(isMasterContext):
      #if it's next to master Context it's referred query will be the first ref query in the traversal
      newtempList.append(firstRefQuery)
    else:
      newtempList.append(lastRefQuery) 
    newtempList.append(prefixContentType+str(prefixIdentifier)[0])
    if(len(newtempList)==6):
      myList.append(newtempList)
  passTempList = list(tempList)
  for i in range(0,len(root)):
    if(re.sub(namespace, '', root.tag) == 'contents'):
      seenContents = 1
      prefixIdentifier = prefixIdentifier*10 + (i+1)
      if(len(prefixContentType)==0):
        prefixContentType = re.sub(namespace, '', root[i].tag)
    newTempList = list(passTempList)
    if(re.sub(namespace, '', root.tag) == 'reportPages' or re.sub(namespace, '', root.tag) == 'promptPages'):
      newTempList.append(re.sub(namespace, '', root.tag))
    if(re.sub(namespace, '', root.tag) =='page'):
      newTempList.append(root.attrib['name'])
      pageNameList.append(root.attrib['name'])
    copyList = list(newTempList)
    getPageItems(namespace,root[i],copyList,myList,lastRefQuery,prefixIdentifier,prefixContentType,pageNameList,firstRefQuery,isMasterContext,seenContents)


def createPagesDF(namespace,root,joiningString,dataItemDict):
  #Extracting data from Layouts section of xml file as page items and storing them as reportPages or promptPages
  reportPages = []
  pageNames = []
  for i in root:
    if re.sub(namespace, '', i.tag) == 'layouts':
      #calling the earlier defined method to get page items which are refered in the order of textItem->dataSource->staticValue/refDataItem
      getPageItems(namespace,i,[],reportPages,"",0,"",pageNames,"",0,0)

  #created dataframe for reportPages/promptPages
  reportPages_df = pd.DataFrame(reportPages, columns = ['reportPages/promptPages', 'page_Name', 'staticValue/refDataItem', 'value','lastRefQuery','prefixIdentifier',])
  
  mappedRefDataItem = []
  for index, row in reportPages_df.iterrows():
    if(row['staticValue/refDataItem'] == 'refDataItem'):
      if(row['lastRefQuery']):
        if(row['lastRefQuery'] +joiningString+ row['value'] in dataItemDict):
          mappedRefDataItem.append(dataItemDict[row['lastRefQuery'] +joiningString+ row['value']])
        else:
          mappedRefDataItem.append('NA')
      else:
        mappedRefDataItem.append('NA')
    else:
      mappedRefDataItem.append("")

  reportPages_df['mappedRefDataItem'] = mappedRefDataItem

  pageNames_df = pd.DataFrame(set(pageNames), columns = ['Page_Name'])
  return reportPages_df,pageNames_df

def getModelPath(namespace,root):
  modelPath = []
  for child in root:
    if(re.sub(namespace, '', child.tag) == 'modelPath'):
      modelPath.append(child.text)
  package = []
  model = []
  if(len(modelPath)):
    pathFromXML = modelPath[0]
    package = (re.findall(r"package\[@name='([^']+)'",pathFromXML))
    model = (re.findall(r"model\[@name='([^']+)'",pathFromXML))

  modelPath_df = pd.DataFrame({'modelPath':modelPath,'package':package,'model':model})
  return modelPath_df