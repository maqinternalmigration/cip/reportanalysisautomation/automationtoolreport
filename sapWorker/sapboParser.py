import requests
import xml.etree.ElementTree as ET
from . import getData as data



class BIPReport:
    def __init__(self, server_name, username, password):
        self.server_name = server_name
        self.username = username
        self.password = password
        self.auth_token = self.get_auth_token()

    def get_auth_token(self):
        headers = {"Content-Type": "application/xml"}
        url = f"http://{self.server_name}:6405/biprws/logon/long"
        xml_request = f"""<attrs xmlns="http://www.sap.com/rws/bip"> 
                                <attr name="userName" type="string">{self.username}</attr>
                                <attr name="password" type="string">{self.password}</attr>
                                <attr name="auth" type="string" possibilities="secEnterprise,secLDAP,secWinAD,secSAPR3">secEnterprise</attr>
                            </attrs>"""

        response = requests.post(url, data=xml_request, headers=headers)

        if response.status_code == 200:
            cookies = response.cookies
            # print("cookies")
        else:
            raise ValueError("Login failed with status code:", response.status_code)

        root = ET.fromstring(response.content)

        logon_token = ""
        for x in root.iter():
          if '{http://www.sap.com/rws/bip}attr' in x.tag:
            if 'name' in x.attrib:
              if x.attrib['name'] == "logonToken":
                logon_token = x.text

        return logon_token


def main(bip_report,outputPath):
   documents_info = data.get_documents_info(bip_report.server_name,bip_report.auth_token,outputPath)
