import os
import sys
import openpyxl
from openpyxl.worksheet.table import Table, TableStyleInfo
from openpyxl.styles import Font
from . import reportComplexityCheck as setComplexity
from pathlib import Path
curPath = ''
if getattr(sys, 'frozen', False):
    # If the application is run as a bundle (e.g. PyInstaller bundle)
    os.chdir(sys._MEIPASS)
else:
    # If the application is run as a script
    # os.chdir(os.path.dirname(Path( __file__ ).parent.absolute()))
    curPath = os.path.join(os.getcwd(),'tableauWorker')
    # os.chdir(r'tableauWorker')

# Format as Table
def createTable(tableName,worksheet,height,lastColumn,startColRow="A1:"):
    size = startColRow + lastColumn + str(worksheet.max_row) 
    tab = Table(displayName=tableName, ref=size)

    # Add a default style with striped rows and banded columns
    style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
    tab.tableStyleInfo = style
    
    worksheet.add_table(tab)
    #worksheet.column_dimensions['C'].width = 100
    
    
# Create a new workbook and add each array to a new worksheets 
def writeToExcel(fileName,outputPath,dataSourceNames,columnAttribute,columnFormula,worksheetNames,dashboardNames,dashboardColumn,LODmeasures,parameterColumn,filterGroupAction,groupMap):
    wb = openpyxl.Workbook()
    ss_sheet = wb["Sheet"]
    ss_sheet.title = "Data Sources"
    wb.create_sheet(title="Columns")
    wb.create_sheet(title="Measures")
    wb.create_sheet(title="Worksheets")
    wb.create_sheet(title="Dashboards")
    wb.create_sheet(title="Dashboard-Worksheet-Columns")
    wb.create_sheet(title="Parameter Columns")
    wb.create_sheet(title="FilterGroupActions")
    
    datasource = wb["Data Sources"]
    column_header = ['Connection Name','Datasource Type','Connection ID','Table Name','Query']
    datasource.append(column_header)
    datasource.Font = Font(size = 24, bold = True)
    for row in dataSourceNames:
        datasource.append(row)
    createTable("DataSources",datasource,len(dataSourceNames),"E")

    columns = wb["Columns"]
    column_header = ['Column Name','Table Name','DataType','Formula']
    columns.append(column_header)
    columns.Font = Font(size = 24, bold = True)
    for row in columnAttribute:
        columns.append(row)
    createTable("TableColumns",columns,len(columnAttribute),"D")

    measures = wb["Measures"]
    column_header = ['Name','Role','Measure','FIXED','EXCLUDE','INCLUDE','LOOKUP','NestedLOD']
    measures.append(column_header)
    measures.Font = Font(size = 24, bold = True)
    for row in columnFormula:
        measures.append(row)
    createTable("Measures",measures,len(columnFormula),"H")

    worksheet = wb["Worksheets"]
    column_header = ['Name','Associated Columns']
    worksheet.append(column_header)
    worksheet.Font = Font(size = 24, bold = True)
    for row in worksheetNames:
        worksheet.append(row)
    createTable("Worksheets",worksheet,len(worksheetNames),"B")
    
    dashboard = wb["Dashboards"]
    column_header = ['Name','Associated Worksheets']
    dashboard.append(column_header)
    dashboard.Font = Font(size = 24, bold = True)
    for row in dashboardNames:
        dashboard.append(row)
    createTable("Dashboards",dashboard,len(columnAttribute),"B")
    
    dashboardColumnRelation = wb["Dashboard-Worksheet-Columns"]
    column_header = ['Datasource','Worksheet','Columns']
    dashboardColumnRelation.append(column_header)
    dashboardColumnRelation.Font = Font(size = 24, bold = True)
    for row in dashboardColumn:
        dashboardColumnRelation.append(row)
    createTable("dashboardColumnRelation",dashboardColumnRelation,len(dashboardColumn),"C")
        
    parameterCol = wb["Parameter Columns"]
    column_header = ['Column Name']
    parameterCol.append(column_header)
    parameterCol.Font = Font(size = 24, bold = True)
    row = 2
    for i in range(len(parameterColumn)):
        parameterCol.cell(row = row + i, column = 1).value = parameterColumn[i]
    createTable("Parameters",parameterCol,len(parameterColumn),"A")
    
    actions = wb["FilterGroupActions"]
    column_header = ['Group','Action']
    mappedGroup = []
    actions.append(column_header)
    actions.Font = Font(size = 24, bold = True)
    for row in filterGroupAction:
        actions.append(row)
        mappedGroup.append(row[0])
    row = len(filterGroupAction)+2
    for key,value in groupMap.items():
        value = value.get('caption')
        if value in mappedGroup:
            continue
        actions.cell(row,column=1).value = value
        row += 1
    createTable("FilterGroupActions",actions,len(filterGroupAction),"B")
    
    #Check complexity of report
    complexityScore = setComplexity.reportComplexity(fileName, dataSourceNames, worksheetNames, columnFormula, LODmeasures, parameterColumn)
    
    try:        
        #Saving Report
        wb.save(os.path.join(outputPath,f'{fileName}.xlsx'))
        print(f"[+] {fileName}.xlsx file created")
        return complexityScore
    except FileNotFoundError:
        print("[-] The file path is invalid.")
        return
    except PermissionError:
        print("[-] The file is open or being used by another process.")
        return

# Create Report Complexity File
def generateComplexityReport(resultQueue,outputPath):
    # Load the workbook containing the macros
    srcPath = os.path.join(curPath,r"Reports Complexity.xlsm")
    destPath = os.path.join(outputPath,"Reports Complexity.xlsm")
    try:
        wb = openpyxl.load_workbook(srcPath,read_only=False, keep_vba=True)
    except FileNotFoundError:
        print("[-] Source File Not Found.")
        return
    except Exception as e:
        print(f"An error occurred while loading the file: {e}")
        return
    # Select the worksheet where you want to append data
    reportComplexity = wb["Reports Complexity"]
    # complexityWorking = wb["ComplexityWorking Manual"]
    reportComplexity.Font = Font(size = 24, bold = True)
    
    
    queueLenght = resultQueue.qsize()
    for i in range(queueLenght):
        currentScore = resultQueue.get()
        # print(currentScore)
        if currentScore is None:
            break
        reportComplexity.append(currentScore)
    
    # Save Report Complexity file    
    createTable("ReportComplexity",reportComplexity,queueLenght,"G","A11:")
    try:
        wb.save(destPath)
        print("[+] Reports Complexity.xlsx file created")
    except FileNotFoundError:
        print("[-] The file path is invalid.")
        return
    except PermissionError:
        print("[-] The file is open or being used by another process.")
        return