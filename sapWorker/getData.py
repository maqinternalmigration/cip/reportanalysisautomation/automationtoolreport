import requests
import xml.etree.ElementTree as ET
import pandas as pd
from . import writeDataToExcel as createExcel


def get_documents_info(server_name,auth_token,outputPath):
        headers = {"X-SAP-LogonToken": auth_token, "Accept": "application/xml"}
        url = f"http://{server_name}:6405/biprws/raylight/v1/documents"
        response = requests.get(url, headers=headers)
        root = ET.fromstring(response.content.decode("utf-8").replace("&amp;", "&"))
        documents = []
        for doc in root.findall('document'):
          doc_id = doc.find('id').text
          cuid = doc.find('cuid').text
          name = doc.find('name').text
          folder_id = doc.find('folderId').text
          
          url_data_provider = f"http://{server_name}:6405/biprws/raylight/v1/documents/{doc_id}/dataproviders"
          response_data_provider = requests.get(url_data_provider, headers=headers) 
          root_data_provider = ET.fromstring(response_data_provider.content.decode("utf-8").replace("&amp;", "&"))
          for dp in root_data_provider.findall('dataprovider'):
            data_provider_id = dp.find('id').text
            data_source_id = "" if dp.find('dataSourceId')==None else dp.find('dataSourceId').text
            data_source_type = "" if dp.find('dataSourceType')==None else dp.find('dataSourceType').text
 
            url_report = f"http://{server_name}:6405/biprws/raylight/v1/documents/{doc_id}/reports"
            response_report = requests.get(url_report, headers=headers)
            root_report = ET.fromstring(response_report.content.decode("utf-8").replace("&amp;", "&"))
            for rp in root_report.findall('report'):
              report_id = rp.find('id').text
              report_name = rp.find('name').text
              documents.append((doc_id,cuid,name,folder_id,data_provider_id,data_source_id,data_source_type,report_id,report_name))
        documents_dataproviders_report_df = pd.DataFrame(documents, columns = ['Document ID', 'CUID', 'Document Name', 'Folder ID','Data Provider ID','Data Source ID','Data Source Type','Report ID','Report Name'])
        Doc_Rep = documents_dataproviders_report_df[['Document ID', 'Report ID']].drop_duplicates()
        # For Query plan
        Doc_Dataprovider = documents_dataproviders_report_df[['Document ID', 'Data Provider ID']].drop_duplicates()

        querystatements = []
        for index, row in Doc_Dataprovider.iterrows():
          doc_id = row['Document ID']
          data_provider_id = row ['Data Provider ID']
          url_data_provider_query = f"http://{server_name}:6405/biprws/raylight/v1/documents/{doc_id}/dataproviders/{data_provider_id}/queryplan"
          response_data_provider_query = requests.get(url_data_provider_query, headers=headers)
          root_data_provider_query = ET.fromstring(response_data_provider_query.content.decode("utf-8").replace("&amp;", "&")) 
          for x in root_data_provider_query.iter():
            if 'statement' in x.tag:
              querystatements.append((doc_id,data_provider_id,x.text))

        document_data_provider_querystatements_df = pd.DataFrame(querystatements, columns = ['Document ID','Data Provider ID','Query'])

        document_report_dict = {}

        document_report_element_metadata = []
        for index, row in Doc_Rep.iterrows():
          doc_id = row['Document ID']
          report_id = row ['Report ID']
          url_report_elements = f"http://{server_name}:6405/biprws/raylight/v1/documents/{doc_id}/reports/{report_id}/elements"
          response_report_elements = requests.get(url_report_elements, headers=headers)
          doc_rep_identifier = "doc_"+str(doc_id)+"rep_"+str(report_id)
          document_report_dict[doc_rep_identifier]=[]
          root_report_elements = ET.fromstring(response_report_elements.content.decode("utf-8").replace("&amp;", "&"))
          for report_el in root_report_elements.findall('element'):
            element_type = ""
            if('type' in report_el.attrib):
              element_type = report_el.attrib['type']
            element_id = report_el.find('id').text
            element_name = ""
            temp_element_name_obj = report_el.find('name')
            if temp_element_name_obj==None:
              element_name = ""
            elif temp_element_name_obj.text[0] == "=":
              element_name = temp_element_name_obj.text[1:]
            else: 
              element_name = temp_element_name_obj.text
            element_parent_id = "" if report_el.find('parentId')==None else report_el.find('parentId').text
            url_report_elements_dataset_metadata = f"http://{server_name}:6405/biprws/raylight/v1/documents/{doc_id}/reports/{report_id}/elements/{element_id}/dataset"
            response_report_elements_dataset_metadata = requests.get(url_report_elements_dataset_metadata, headers=headers)

            data_object_id = ""
            data_object_type = ""
            data_object_value = ""
            hasEqualsTo = 0
            if(response_report_elements_dataset_metadata.status_code == 200):
              root_report_elements_dataset_metadata = ET.fromstring(response_report_elements_dataset_metadata.content)
              for mt_data in root_report_elements_dataset_metadata.findall('metadata'):
                for mt_values in mt_data.findall('value'):
                  if mt_values.text == None:
                    data_object_value = "" 
                  elif mt_values.text[0] == "=":
                    data_object_value = mt_values.text[1:]
                    hasEqualsTo = 1
                  else: 
                    data_object_value = mt_values.text

                  data_object_id = ""
                  data_object_type = ""
                  if('dataObjectId' in mt_values.attrib):
                    data_object_id = mt_values.attrib['dataObjectId']
                  if('type' in mt_values.attrib):
                    data_object_type = mt_values.attrib['type']
                  document_report_dict[doc_rep_identifier].append((doc_rep_identifier,doc_id,report_id,element_id,element_name,element_type,element_parent_id,response_report_elements_dataset_metadata.status_code,data_object_id,data_object_type,hasEqualsTo,data_object_value))
                  document_report_element_metadata.append((doc_rep_identifier,doc_id,report_id,element_id,element_name,element_type,element_parent_id,response_report_elements_dataset_metadata.status_code,data_object_id,data_object_type,hasEqualsTo,data_object_value))
            else:
              document_report_dict[doc_rep_identifier].append((doc_rep_identifier,doc_id,report_id,element_id,element_name,element_type,element_parent_id,response_report_elements_dataset_metadata.status_code,data_object_id,data_object_type,hasEqualsTo,data_object_value))
              document_report_element_metadata.append((doc_rep_identifier,doc_id,report_id,element_id,element_name,element_type,element_parent_id,response_report_elements_dataset_metadata.status_code,data_object_id,data_object_type,hasEqualsTo,data_object_value))

        document_report_element_metadata_df = pd.DataFrame(document_report_element_metadata, columns = ['doc_rep_identifier','doc_id','report_id','element_id','element_name','element_type','element_parent_id','response_report_elements_dataset_metadata.status_code','data_object_id','data_object_type','hasEqualsTo','data_object_value'])
        doc_rep_get_stats_for_complexity_result = document_report_element_metadata_df.groupby(['doc_id', 'report_id'],as_index=False).agg({'element_id': 'nunique', 'data_object_id': 'nunique', 'hasEqualsTo': 'sum'})
        doc_get_stats_for_complexity_result = doc_rep_get_stats_for_complexity_result.groupby('doc_id',as_index=False).agg({'report_id': 'nunique', 'element_id': 'sum', 'data_object_id': 'sum', 'hasEqualsTo': 'sum'})
        url_universe = f"http://{server_name}:6405/biprws/raylight/v1/universes?limit=49"
        response_universe = requests.get(url_universe, headers=headers)
        root_response_universe = ET.fromstring(response_universe.content)
        universe_content = []
        for univs in root_response_universe.findall('universe'):
          universe_id = univs.find('id').text
          universe_name = univs.find('name').text
          url_universe_details = f"http://{server_name}:6405/biprws/raylight/v1/universes/{universe_id}?aggregated=false"
          response_universe_details = requests.get(url_universe_details, headers=headers)
          root_response_universe_details = ET.fromstring(response_universe_details.content)
          for outlins in root_response_universe_details.findall('outline'):
            for folds in outlins.findall('folder'):
              folder_name = "" if folds.find('name').text == None else folds.find('name').text
              for itms in folds.findall('item'):
                item_type = ""
                if 'type' in itms.attrib:
                  item_type = itms.attrib['type']
                item_data_type = ""
                if 'dataType' in itms.attrib:
                  item_data_type = itms.attrib['dataType']
                
                item_name = "" if itms.find('name').text == None else itms.find('name').text
                item_id = "" if itms.find('id').text == None else itms.find('id').text
                universe_content.append((universe_id,universe_name,folder_name,item_type,item_data_type,item_name,item_id))

        doc_data_unique_df = documents_dataproviders_report_df.groupby(['Document ID','Document Name'],as_index=False).agg({'Report ID': 'nunique', 'Data Provider ID': 'nunique','Data Source ID': 'nunique'})
        doc_merge_get_stats_for_complexity_result = pd.merge(doc_get_stats_for_complexity_result, doc_data_unique_df,  left_on='doc_id', right_on='Document ID')
        doc_merge_get_stats_for_complexity_result = doc_merge_get_stats_for_complexity_result.drop(['Document ID','report_id'], axis=1)
        doc_merge_get_stats_for_complexity_result = doc_merge_get_stats_for_complexity_result.rename(columns={'doc_id': 'Document ID', 'element_id': 'Element ID','data_object_id': 'Data Object ID', 'hasEqualsTo': 'Formulae'})
        doc_merge_get_stats_for_complexity_result_order = ["Document ID",	"Document Name",	"Element ID",	"Data Object ID",	"Formulae",	"Report ID",	"Data Provider ID",	"Data Source ID"]
        doc_merge_get_stats_for_complexity_result = doc_merge_get_stats_for_complexity_result.reindex(columns=doc_merge_get_stats_for_complexity_result_order)
        # print(doc_merge_get_stats_for_complexity_result)
        universe_content_df = pd.DataFrame(universe_content, columns = ['universe_id','universe_name','folder_name','item_type','item_data_type','item_name','item_id'])

        createExcel.to_excel(documents_dataproviders_report_df,document_report_element_metadata_df,universe_content_df,document_data_provider_querystatements_df,doc_merge_get_stats_for_complexity_result,outputPath)
        
        # with pd.ExcelWriter("BOBJ Output.xlsx") as writer:
        #   documents_dataproviders_report_df.to_excel(writer, sheet_name="doc_dataproviders_report", index=False)
        #   document_report_element_metadata_df.to_excel(writer, sheet_name="doc_rep_ele_metadata", index=False)
        #   universe_content_df.to_excel(writer, sheet_name="universe_content", index=False)
        #   document_data_provider_querystatements_df.to_excel(writer, sheet_name="doc_data_provider_query", index=False)
    
        # complexity_sheet_name = 'Document Output'
        # complexity_workbook_name = 'BOBJ Complexity Report.xlsm'
        # createExcel.save_complexity_report(doc_merge_get_stats_for_complexity_result,complexity_sheet_name,complexity_workbook_name)
    