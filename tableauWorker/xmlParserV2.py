from lxml import etree
import os
from . import getData as data
from . import writeDataToExcel as createExcel
# import threading
import queue
# import math

def processTWBFiles(folderPath,outputPath,fileName,resultQueue):
    try:
        tree = etree.parse(os.path.join(folderPath, fileName))
    except etree.XMLSyntaxError as e:
        print('[-] Error parsing file:', str(os.path.join(folderPath, fileName)), str(e))
        return
    root = tree.getroot()

    # get the name of table file
    # outputFileName = data.getTableauName(root)
    # if outputFileName == "":
    outputFileName = fileName[:-4]
    
    # get datasources details
    dataSourceNames = data.getDatasources(root)
    
    #get list of worksheets
    worksheetNames,worksheetColumnMap = data.getWorksheets(root)
    
    # extract column from <column> tag and return key value pair of columnID : columnTree
    columnData = data.extractColumn(root)
    
    # Find Formula under calculations of <column> tag and it depends on result from extractColumn(root)
    columnFormula,calculatedColumns = data.findColumnFormula(columnData)
    
    # Get list of dashboards
    dashboardNames,dashboardNamesExpanded = data.getDashboards(root)
    
    # get columnName,ParentName,Type in 2D Array format
    columnNames = data.getColumns(root)
    
    # print(columnNames)
    # Clean Measures definition
    data.formulaCleaner(columnData,columnFormula,2)
    data.formulaCleaner(columnData,calculatedColumns,3)
    data.mergeCalculatedColumnsWithColumnsList(columnNames,calculatedColumns)
    # Get count of LOD measures used in measure definition
    LODmeasures = data.countLOD(columnFormula)
    
    # get list of parameters
    parameterColumn = data.getParameters(root)
    
    # get list of acions
    actionMap = data.getActions(root)
    groupMap = data.getGroups(root)
    filterGroupAction = data.getAndMapFilter(root,actionMap,groupMap)
    
    dashboardColumn = data.dashboardColumnRelation(dashboardNamesExpanded,worksheetColumnMap)
    
    # Get Complexity Score for data points of Tableau report
    complexityScore = 0
    complexityScore = createExcel.writeToExcel(outputFileName,outputPath,dataSourceNames,columnNames,columnFormula,worksheetNames,dashboardNames,dashboardColumn,LODmeasures,parameterColumn,filterGroupAction,groupMap)
    
    outputList = [complexityScore, len(dataSourceNames), len(worksheetNames), len(columnFormula), LODmeasures, len(parameterColumn)]
    outputList.insert(0,fileName)
    resultQueue.put(outputList)
   
# def workerRun(folderPath,outputPath,fileQueue,i,execStat,resultQueue):
#     while True:
#         file = fileQueue.get()
#         if file is None:
#             break
#         processTWBFiles(folderPath,outputPath,file,resultQueue)
#         execStat[i].append(file)
#         print(f"Executed by worker {i}")
    
def main(tableauQueue):
    if tableauQueue.qsize() == 0:
        return
    # execStat={}
    # global outputPath
    
    
    # Define Queues 
    
    # fileQueue = queue.Queue()
    complexityQueue = queue.Queue()
    
    # Fetch all files from folder and store in Queue

    # for file in os.listdir(folderPath):
    #     if file.endswith('.twb'):
    #         fileQueue.put(file)
    
    # Call the processTWBFiles() for each function
    outputPath = ""
    queueLenght = tableauQueue.qsize()
    for i in range(queueLenght):
        filePath = tableauQueue.get()
        folderPath = os.path.dirname(filePath)
        fileName = os.path.basename(filePath)
        outputPath = os.path.join(folderPath, "tableauOutput")
        if not os.path.exists(folderPath):
            print(f"[-]{folderPath} Not Exist")
        if not os.path.exists(outputPath):
            try:
                os.mkdir(outputPath)
            except OSError:
                print(f"[-]Can't Create output directory on {outputPath}")
                return
        processTWBFiles(folderPath,outputPath,fileName,complexityQueue)

    # num_workers = math.ceil(1)
    
    # workers = [threading.Thread(target=workerRun, args=(folderPath,outputPath,fileQueue,i,execStat,resultQueue)) for i in range(num_workers)]

    # # add a None signal for each worker
    # count = 0
    # for worker in workers:
    #     fileQueue.put(None)
    #     execStat[count] = []
    #     count +=1
    # # start all workers
    # for worker in workers:
    #     worker.start()

    # # wait for all workers to finish
    # for worker in workers:
    #     worker.join()
    # print(execStat)
    # workerResult = worker.result()
    # print(resultQueue)
    createExcel.generateComplexityReport(complexityQueue,outputPath)

    
if __name__ == "__main__":
    main()