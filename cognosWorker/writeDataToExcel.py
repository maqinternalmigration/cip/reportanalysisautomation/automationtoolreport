import pandas as pd
import os
def export_to_excel(filename,outputPath,modelPath_df,df_query_dataItems,df_promptPages,df_reportPages,df_pageNames):
  exportFileName = os.path.join(outputPath,filename[:-4]+".xlsx")
  with pd.ExcelWriter(exportFileName) as writer:
    
      # use to_excel function and specify the sheet_name and index
      # to store the dataframe in specified sheet
      modelPath_df.to_excel(writer, sheet_name="Model Path", index=False)
      df_query_dataItems.to_excel(writer, sheet_name="Query Data Items", index=False)
      df_pageNames.to_excel(writer, sheet_name="Pages", index=False)
      df_promptPages.to_excel(writer, sheet_name="Prompt Page", index=False)
      df_reportPages.to_excel(writer, sheet_name="Report Pages", index=False)
  print(f"[+] {filename[:-4]}.xlsx file created")