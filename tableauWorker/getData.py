import re


# Get Name of Datasources
def getDatasources(root):
    dataSourceNames = []
    dataSourceTypes = {}
    
    dataSources = root.xpath("//named-connection")
    sqlQuery = getCustomSQLQueries(root)
    
    for dataSource in dataSources:
        connection = dataSource.xpath("connection")
        dataSourceTypes[dataSource.get('name')] = [dataSource.get('caption'),connection[0].get('class')]
    
    for tup in sqlQuery:
        tup = list(tup)
        dataSourceNames.append(dataSourceTypes[tup[0]] + tup)
    return getDataSourceSQLProxy(root,dataSourceNames)

# Get Queries
def getCustomSQLQueries(root):
    sqlQuery = set()
    sqlQueries = root.xpath("//datasources//relation")
    for query in sqlQueries:
        if 'connection' in query.attrib:
            queryText = query.text if query.text != None and len(query.text.strip()) != 0 else ""
            sqlQuery.add((query.get('connection'),query.get('name'),queryText))
    return sqlQuery

def getDataSourceSQLProxy(root,dataSourceNames):
    dataSources = root.xpath("//datasources/datasource")

    for dataSource in dataSources:
        connType = dataSource.xpath("connection")[0].get('class') if len(dataSource.xpath("connection"))>0 else ""
        if connType != "sqlproxy":
            continue
        id = dataSource.get('name')
        sqlProxy = dataSource.xpath("repository-location")[0]
        # print(sqlProxy.attrib)
        dataSourceNames.append([sqlProxy.get('derived-from'),connType,id,sqlProxy.get('id'),""])
    return dataSourceNames


# Get Name of Worksheets
def getWorksheets(root):
    worksheetColumnMap = {}
    worksheetNames = []
    worksheets = root.xpath("//worksheet")
    for worksheet in worksheets:
        columns = worksheet.xpath(".//column")
        columnName = set()
        for column in columns:
            columnName.add(column.get('caption') if 'caption' in column.attrib else column.get('name'))
        columnsInWorksheet = ", ".join([f"{column}" for column in columnName])
        worksheetNames.append([worksheet.get('name'),columnsInWorksheet])
        worksheetColumnMap[worksheet.get('name')] = columnsInWorksheet
    return worksheetNames,worksheetColumnMap


# Get Name of Dashboards
def getDashboards(root):
    dashboardNames = []
    dashboardNamesExpanded = []
    dashboards = root.xpath("//dashboards/dashboard")
    for dashboard in dashboards:
        zones = dashboard.xpath(".//zone")
        worksheetSet =  set()
        for zone in zones:
            if 'name' in zone.attrib:
                worksheetSet.add(zone.get('name'))
        for worksheet in worksheetSet:
            dashboardNamesExpanded.append([dashboard.get('name'),worksheet])
        worksheets = ", ".join([f"{worksheet}" for worksheet in worksheetSet])
        dashboardNames.append([dashboard.get('name'),worksheets])
    return dashboardNames,dashboardNamesExpanded

def dashboardColumnRelation(dashboardNames,worksheetColumnMap):
    dashboardColumn = dashboardNames
    for i in range(len(dashboardColumn)):
        dashboardColumn[i].append(worksheetColumnMap[dashboardColumn[i][1]])
    return dashboardColumn
# Get Name of File
def getTableauName(root):
    fileName = root.xpath("//repository-location")
    if fileName:
        return fileName[0].get('id')
    return ""


# # Extract the columns and store it's corresponding element tree in dictonary 
# Acts as a helper function for BasicColumnAttributes
def extractColumn(root):
    columnData = {}
    
    for elements in root:
        columns = elements.xpath(".//column")
        for column in columns:
            if 'name' and 'role' in column.attrib and column.attrib['name'] not in columnData:
                columnData[column.attrib['name']] = column
    return columnData


# From the column details dictonary extracting required compnenets and storing it in 2D matrix, which enables easy writng to xlxs or csv file
def getColumns(root):
    columnNames = []
    columnsMap = {}
    # for elements in root:
    datasources = root.xpath("/workbook/datasources/datasource")
    for datasource in datasources:
        # for tags in x:
        #     print(tags.attrib)
        sourceTableName = datasource.xpath("./connection")[0].get('dbname') if len(datasource.xpath("./connection")) > 0 else ""
        print(sourceTableName)
        columns = datasource.xpath(".//metadata-record")
        for column in columns:
        #     # print(column.attrib)
            if column.get('class') == "column" and column.xpath(".//parent-name")[0].text != '[Extract]':
                columnsMap[column.xpath(".//remote-name")[0].text] = column
                TableName = sourceTableName if column.xpath(".//parent-name")[0].text == "[sqlproxy]" and sourceTableName!="" else column.xpath(".//parent-name")[0].text
                columnNames.append([column.xpath(".//remote-name")[0].text,TableName,column.xpath(".//local-type")[0].text])
    return columnNames


def BasicColumnAttributes(columnData):
    columnAttribute = []
    for column in columnData:
        columnName = columnData[column].get('caption') if 'caption' in columnData[column].attrib else columnData[column].get('name')
        columnAttribute.append([columnName,columnData[column].get('datatype'),columnData[column].get('role'),columnData[column].get('type')])
    return columnAttribute


# For each column check if it has calculation tag in it. If yes extract it's formula
def findColumnFormula(columnData):
    calculatedColumns =[]
    columnCalculations = {}
    for column in columnData:
        calculation = columnData[column].xpath(".//calculation")
        if calculation:
            columnCalculations[column] = calculation[0].get('formula') if calculation[0].get('formula') else calculation[0].get('column')
    columnFormula =[]
    columnHavingFormulaList = []
    for calculation in columnCalculations:
        if calculation in columnData:
            columnName = columnData[calculation].get('caption') if 'caption' in columnData[calculation].attrib else columnData[calculation].get('name')
            role = columnData[calculation].get('role')
            columnID= columnData[calculation].get('name')
            columnHavingFormulaList.append([columnName,columnID])
            if role == "dimension":
                dataType = columnData[calculation].get('datatype')
                calculatedColumns.append([columnName,"",dataType,columnCalculations[calculation]])
            else:
                columnFormula.append([columnName,role,columnCalculations[calculation]])
    return columnFormula,calculatedColumns


# Cleaning Formula field 
def formulaCleaner(columnData,columnFormula,formulaIndex):
    regex = r'\[([^\]]+)\]'                                                                             
    pattern = re.compile(regex)
    
    for i in range(len(columnFormula)):
        calcPattern = (pattern.findall(columnFormula[i][formulaIndex]))
        
        for id in calcPattern:
            id = '['+id+']'
            if id in columnData and columnData[id].get('caption'):
                columnName = '['+columnData[id].get('caption')+']'
                columnFormula[i][formulaIndex] = columnFormula[i][formulaIndex].replace(id,columnName)

def formulaDepth(formula):
    depth = 0
    count = 0
    for match in re.finditer(r'[{}]', formula):
        if match.group() == '{':
            count += 1
            depth = max(depth, count)
        else:
            count -= 1
    return depth


# Get count of LOD in Measures
# Get total LOD in report
def countLOD(columnFormula):
    LODmeasures = 0
    
    columnFormula.append(['Total','-----','-----',0,0,0,0,0,0])
    for i in range(len(columnFormula)-1):
        columnFormula[i].append(columnFormula[i][2].upper().count("FIXED"))
        columnFormula[i].append(columnFormula[i][2].upper().count("EXCLUDE"))
        columnFormula[i].append(columnFormula[i][2].upper().count("INCLUDE"))
        columnFormula[i].append(columnFormula[i][2].upper().count("LOOKUP"))
        LODDepth = formulaDepth(columnFormula[i][2])
        LODDepth = LODDepth-1 if LODDepth > 1 else 0
        columnFormula[i].append(LODDepth)
        columnFormula[len(columnFormula)-1][3] += columnFormula[i][3]
        columnFormula[len(columnFormula)-1][4] += columnFormula[i][4]
        columnFormula[len(columnFormula)-1][5] += columnFormula[i][5]
        columnFormula[len(columnFormula)-1][6] += columnFormula[i][6]
        columnFormula[len(columnFormula)-1][7] += columnFormula[i][7]
    countFEIL = int(columnFormula[len(columnFormula)-1][3] > 0) + int(columnFormula[len(columnFormula)-1][4]>0) + int(columnFormula[len(columnFormula)-1][5] > 0)+ int(columnFormula[len(columnFormula)-1][6] > 0) + columnFormula[len(columnFormula)-1][7]
    columnFormula[len(columnFormula)-1][8] = countFEIL
    for i in range(len(columnFormula)-1):
        LODmeasures +=  columnFormula[i][2].upper().count("FIXED") + columnFormula[i][2].upper().count("EXCLUDE") +columnFormula[i][2].upper().count("INCLUDE") +columnFormula[i][2].upper().count("LOOKUP")
    return countFEIL

# Get list of Parameters
def getParameters(root):
    parameterColumn = []
    datasources = root.xpath("//datasources/datasource")
    for datasource in datasources:
        if datasource.get('name') == 'Parameters':
            for column in datasource.xpath('column'):
                parameterColumn.append(column.get('name'))
    return parameterColumn

# Get Actions
def getActions(root):
    actionMap = {}
    actions = root.xpath("//actions/action")
    for action in actions:
        actionMap[action.get('name')] = action
    return actionMap

def getGroups(root):
    groupMap = {}
    groups = root.xpath('//datasources//group')
    for group in groups:
        groupMap[group.get('name')] = group
    return groupMap


def getAndMapFilter(root,actionMap,groupMap):
    filterGroupAction = set()
    user = '{http://www.tableausoftware.com/xml/user}'
    # print(user)
    filters = root.xpath("//worksheets//filter")
    for filter in filters:
        group = (filter.get('column').split('].')[1]) if filter.get('column') else None
        action = (filter.xpath('groupfilter')[0].get(user+'ui-action-filter')) if filter.xpath('groupfilter') else None
        group = groupMap.get(group)
        action = actionMap.get(action)
        if action != None and group != None :
            filterGroupAction.add((group.get('caption'),action.get('caption')))
    return filterGroupAction

def mergeCalculatedColumnsWithColumnsList(columnNames,calculatedColumns):
    for columnDeatails in calculatedColumns:
        columnNames.append(columnDeatails)
    