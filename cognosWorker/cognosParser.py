from . import getData as data
from . import writeDataToExcel as createExcel
import os
import xml.etree.ElementTree as et

def myCode(filePath,filename,directory):
  outputPath = os.path.join(directory,"cognosOutput")
  if not os.path.exists(outputPath):
    try:
      os.mkdir(outputPath)
    except OSError:
      print(f"[-]Can't Create output directory on {outputPath}")
      return
  try:
    tree=et.parse(filePath)
  except et.XMLSyntaxError as e:
    print('[-] Error parsing file:', str(filePath), str(e))
    return
  root=tree.getroot() 
  print(data.hello)
  namespace = data.getNamespace(root)
  if(namespace == None):
    namespace = ''
  else:
    namespace = '{'+namespace+'}'

  #call code to create query Data Items page
  df_query_dataItems = data.QueryDataItemsPage(namespace,root)
  
  #creating a dictionary of query_name joined with dataItem_name using joiningString mapped to Data Objects
  dataItemDict = {}
  joiningString = '##JOINING##'
  for index, row in df_query_dataItems.iterrows():
    if(row['query_name']+joiningString+row['dataItem_name'] not in dataItemDict):
      dataItemDict[row['query_name']+joiningString+row['dataItem_name']]= {row['data_objects']}
    dataItemDict[row['query_name']+joiningString+row['dataItem_name']].add(row['data_objects'])
  dataItemDict
  
  #updating [*] type objects to their referenced paths
  for index, row in df_query_dataItems.iterrows():
    obj = row['data_objects'].split(".")
    if(len(obj)==1):
      ProbableQueryName = row['query_name']
      ProbableDataItemName = obj[0]
      if(ProbableQueryName+joiningString+ProbableDataItemName in dataItemDict):
        row['data_objects'] = dataItemDict[ProbableQueryName+joiningString+ProbableDataItemName]
  df_query_dataItems = df_query_dataItems.explode('data_objects')

  #updating dictionary
  dataItemDict = {}
  joiningString = '##JOINING##'
  for index, row in df_query_dataItems.iterrows():
    # print(row)
    if(row['query_name']+joiningString+row['dataItem_name'] not in dataItemDict):
      dataItemDict[row['query_name']+joiningString+row['dataItem_name']]= {row['data_objects']}
    dataItemDict[row['query_name']+joiningString+row['dataItem_name']].add(row['data_objects'])
  dataItemDict

  #updating [*][*] type objects to their referenced paths
  for index, row in df_query_dataItems.iterrows():
    obj = row['data_objects'].split(".")
    if(len(obj)==2):
      ProbableQueryName = obj[0]
      ProbableDataItemName = obj[1]
      if(ProbableQueryName+joiningString+ProbableDataItemName in dataItemDict):
        row['data_objects'] = dataItemDict[ProbableQueryName+joiningString+ProbableDataItemName]
  df_query_dataItems = df_query_dataItems.explode('data_objects')

  #updating dictionary
  dataItemDict = {}
  joiningString = '##JOINING##'
  for index, row in df_query_dataItems.iterrows():
    if(row['query_name']+joiningString+row['dataItem_name'] not in dataItemDict):
      dataItemDict[row['query_name']+joiningString+row['dataItem_name']]= {row['data_objects']}
    dataItemDict[row['query_name']+joiningString+row['dataItem_name']].add(row['data_objects'])

  #updating [*][*] type objects to their referenced paths for 2nd time because the earlier referenced objects could also be of [*].[*] type
  for index, row in df_query_dataItems.iterrows():
    obj = row['data_objects'].split(".")
    if(len(obj)==2):
      ProbableQueryName = obj[0]
      ProbableDataItemName = obj[1]
      if(ProbableQueryName+joiningString+ProbableDataItemName in dataItemDict):
        row['data_objects'] = dataItemDict[ProbableQueryName+joiningString+ProbableDataItemName]
  df_query_dataItems = df_query_dataItems.explode('data_objects')

  #updating dictionary again
  dataItemDict = {}
  joiningString = '##JOINING##'
  for index, row in df_query_dataItems.iterrows():
    if(row['query_name']+joiningString+row['dataItem_name'] not in dataItemDict):
      dataItemDict[row['query_name']+joiningString+row['dataItem_name']]= {row['data_objects']}
    dataItemDict[row['query_name']+joiningString+row['dataItem_name']].add(row['data_objects'])
  # dataItemDict

  #end of creating Query Data Items Page 
  reportPages_df,df_pageNames = data.createPagesDF(namespace,root,joiningString,dataItemDict)
  
  reportPages_df = reportPages_df.explode('mappedRefDataItem')
  df_reportPages = reportPages_df[reportPages_df['reportPages/promptPages'] =='reportPages']
  df_reportPages.drop(['reportPages/promptPages'], axis=1 ,inplace = True)
  df_promptPages = reportPages_df[reportPages_df['reportPages/promptPages'] =='promptPages']
  df_promptPages.drop(['reportPages/promptPages'], axis=1 ,inplace = True)

  #get model path dataframe
  modelPath_df = data.getModelPath(namespace,root)
  
  createExcel.export_to_excel(filename,outputPath,modelPath_df,df_query_dataItems,df_promptPages,df_reportPages,df_pageNames)



def mainCode(cognosQueue):
  if cognosQueue.qsize() == 0:
        return
  # iterate over files in the directory
  file_count = cognosQueue.qsize()
  for i in range(file_count):
    filePath = cognosQueue.get()
    directory = os.path.dirname(filePath)
    filename = os.path.basename(filePath)
    myCode(filePath,filename,directory)



# directory = '.'
# mainCode(directory)