import pandas as pd
import openpyxl
from openpyxl.utils.dataframe import dataframe_to_rows
import os
import sys
curPath = ""
if getattr(sys, 'frozen', False):
    # If the application is run as a bundle (e.g. PyInstaller bundle)
    os.chdir(sys._MEIPASS)
else:
    # If the application is run as a script
    curPath = os.path.join(os.getcwd(),'sapWorker')
    pass

def to_excel(documents_dataproviders_report,document_report_element_metadata,universe_content,document_data_provider_querystatements,doc_merge_get_stats_for_complexity_result,outputPath):
        
        documents_dataproviders_report_df=pd.DataFrame(documents_dataproviders_report)
        document_report_element_metadata_df=pd.DataFrame(document_report_element_metadata)
        universe_content_df =pd.DataFrame(universe_content)
        document_data_provider_querystatements_df=pd.DataFrame(document_data_provider_querystatements)
        
        output_workbook_name ="BOBJ Output.xlsx"
        output_workbook=os.path.join(outputPath,output_workbook_name)
        with pd.ExcelWriter(output_workbook) as writer:
          documents_dataproviders_report_df.to_excel(writer, sheet_name="doc_dataproviders_report", index=False)
          document_report_element_metadata_df.to_excel(writer, sheet_name="doc_rep_ele_metadata", index=False)
          universe_content_df.to_excel(writer, sheet_name="universe_content", index=False)
          document_data_provider_querystatements_df.to_excel(writer, sheet_name="doc_data_provider_query", index=False)

        complexity_sheet_name = 'Document Output'
        complexity_workbook_name = os.path.join(curPath,r'BOBJ Complexity Report.xlsm')
        save_complexity_report(doc_merge_get_stats_for_complexity_result,complexity_sheet_name,complexity_workbook_name,outputPath)


def save_complexity_report(complexity_df,complexity_sheet_name,complexity_workbook_name,outputPath):
    workbook = openpyxl.load_workbook(complexity_workbook_name, keep_vba=True)
    if complexity_sheet_name not in workbook.sheetnames:
        workbook.create_sheet(complexity_sheet_name)

    worksheet = workbook[complexity_sheet_name]
    worksheet.delete_rows(1, worksheet.max_row)
    rows = dataframe_to_rows(complexity_df, index=False, header=True)
    for r_idx, row in enumerate(rows, 1):
        for c_idx, value in enumerate(row, 1):
            worksheet.cell(row=r_idx, column=c_idx, value=value)
    workbook.save(os.path.join(outputPath, r'BOBJ Complexity Report.xlsm'))