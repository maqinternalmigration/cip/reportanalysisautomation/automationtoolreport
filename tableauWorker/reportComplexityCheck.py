
# Set final Complexity for particular data point
def check(complexity,finalComplexity):    
    if complexity>finalComplexity:
        finalComplexity = complexity
    else:
        complexity
    return finalComplexity


# Set Measure Level Complexity
def setMeasureLevelComplexity(measureCount,LODcount):
    # LOD Complexity
    LODComplexity = 0
    # if LODcount > 4:
    #     LODComplexity = 3
    # elif LODcount > 1:
    #     LODComplexity = 2
    # elif LODcount > 0:
    #     LODComplexity = 1
    # else:
    #     LODComplexity = 0
    if LODcount == 0:
        LODComplexity = 0
    elif LODcount <2:
        LODComplexity = 1
    elif LODcount < 5:
        LODComplexity = 2
    else:
        LODComplexity = 3

    # Measures Complexity
    measureComplexity = 0
    if LODComplexity == 0:
        if measureCount < 16:
            measureComplexity = 1
        elif measureCount < 40:
            measureComplexity = 2
        else:
            measureComplexity = 3
    elif LODComplexity == 1:
        if measureCount > 30 :
            measureComplexity = 3
        else:
            measureComplexity=2
    elif LODComplexity == 2:
        if measureCount > 26 :
            measureComplexity = 2
        else:
            measureComplexity = 3
    else:
        measureComplexity = 3
    
    return measureComplexity


# Set Measure - Parameter Level Complexity
def setMeasureParameterLevelComplexity(parameterComplexity,measureLevelComplexity):
    measureParameterComplexity = 0
    if parameterComplexity == 1 and measureLevelComplexity == 1:
        measureParameterComplexity = 1
    elif parameterComplexity == 1 and measureLevelComplexity == 2:
        measureParameterComplexity = 2
    elif parameterComplexity == 1 and measureLevelComplexity == 3:
        measureParameterComplexity = 3
    elif parameterComplexity == 2 and (measureLevelComplexity == 1 or measureLevelComplexity == 2) :
        measureParameterComplexity = 2
    elif parameterComplexity == 2 and measureLevelComplexity == 3:
        measureParameterComplexity = 3
    elif parameterComplexity == 3 and measureLevelComplexity == 1:
        measureParameterComplexity = 2
    else:
        measureParameterComplexity = 3
    
    return measureParameterComplexity


#Set Report Complexity
def setFinalComplexity(finalComplexity,measureParameterLevelComplexity):
    reportComplexity = ""
    if finalComplexity == 1 and measureParameterLevelComplexity == 1:
        reportComplexity = "Simple"
    elif finalComplexity == 1 and measureParameterLevelComplexity >= 2:
        reportComplexity = "Medium"
    elif finalComplexity == 2 and (measureParameterLevelComplexity == 2 or measureParameterLevelComplexity == 1):
        reportComplexity = "Medium"
    elif finalComplexity == 2 and measureParameterLevelComplexity == 3:
        reportComplexity = "High"
    elif finalComplexity == 3 and measureParameterLevelComplexity == 1:
        reportComplexity = "Medium"
    else:
        reportComplexity = "High"
        
    return reportComplexity


# Check Report Complexity
def reportComplexity(fileName, dataSourceNames, worksheetNames, columnFormulas, LODmeasures, parameters):
    sourcesCount = len(dataSourceNames)
    sheetCount = len(worksheetNames)
    measureCount = len(columnFormulas)
    LODcount = LODmeasures
    parameterCount = len(parameters)
    # actionCount = len(actions)
    
    complexity = 0
    finalComplexity = 0
    measureLevelComplexity = 0
    measureParameterLevelComplexity = 0
    # measureParameterActionComplexity = 0
    
    # Data Sources Complexity
    if sourcesCount < 2:
        complexity = 1
    elif sourcesCount < 4:
        complexity = 2
    else:
        complexity = 3
    
    finalComplexity = complexity
    complexity=0

    # Actions Complexity
    # if actionsCount < 11:
    #     complexity = 1
    # elif actionsCount < 20:
    #     complexity = 2
    # else:
    #     complexity = 3
    
    # finalComplexity = check(complexity,finalComplexity)
    # complexity=0
    
    
    # WorkSheet Complexity
    if 	sheetCount<9:
        complexity = 1
    elif sheetCount<17:
        complexity=2
    else:
        complexity=3
    
    finalComplexity = check(complexity,finalComplexity)
    print("Final : " + str(finalComplexity))
    complexity=0
    
     
    # Measure Level Complexity
    measureLevelComplexity = setMeasureLevelComplexity(measureCount,LODcount)
    print("Measure Level Complexity :- " + str(measureLevelComplexity))
    
    
    # Parameter Complexity
    if parameterCount < 1:
        parameterComplexity = 1
    elif parameterCount < 3:
        parameterComplexity = 2
    else:
        parameterComplexity = 3
    print("Parameter Complexity :- " + str(parameterComplexity))
    
    
    # Measure - Parameter Level Complexity
    measureParameterLevelComplexity = setMeasureParameterLevelComplexity(parameterComplexity,measureLevelComplexity)
    print("Measure - Parameter Level Complexity :- " + str(measureParameterLevelComplexity))
    
    
    # return setFinalComplexity(finalComplexity,measureParameterLevelComplexity)
    return setFinalComplexity(finalComplexity,measureParameterLevelComplexity)

